# Exame frontend Zerum

Aplicação que exibe uma lista de tarefas. É possível adicionar novas tarefas, reordenar ordem de tarefas e marcá-las como completas.

## Primeiros passos

Clone este repositório e siga as instruções a seguir para rodar este projeto localmente.

### Pré-requisitos

É necessário baixar a versão mais recente do `npm` e do `nodejs`. Caso utilize o Windows, utilize o instalador [deste site](https://www.npmjs.com/get-npm).

### Instalação
Abra o prompt e navegue até a pasta que contenha o arquivo *package.json* baixe as dependências do projeto com o comando:
```
npm install
```
Em seguinda, gere a versão de produção da aplicação com:
```
npm run build
```

Estes processos podem demorar um pouco.

## Execute os testes

Abra dois prompts e navegue com ambos para a pasta `app`.

Em um deles, inicie o servidor com o comando:
```
.\node_modules\.bin\json-server --watch .\tests\test-database.json
```
Com o outro, rode os testes com:

```
npm run test:unit
```

Após os testes terminarem, finalize o servidor.
### App.vue

Verifica se a aplicação consegue renderizar ese consegue carregar seus componentes.

### Notification.vue

Verifica se uma mensagem de erro é renderiza somente se um evento é acionado.

### List.vue

Verifica se consegue listar o banco de dados com pelo menos um elemento, se ele consegue atualizar o elemento de id 1 e se consegue deletar o elemento de id 1.

### Insert.vue

Verifica se consegue inserir uma tarefa no banco.

## Fazendo deploy

Abra dois prompts e navegue com ambos para a pasta `app`.

Em um deles, inicie o servidor com o comando:
```
.\node_modules\.bin\json-server --watch ..\database.json
```

Com o outro, rode a aplicação localmente com:
```
.\node_modules\.bin\serve -s dist
```
Aparecerá uma mensagem *serving* e o endereço local http para visualizar a aplicação. Copie este endereço e cole-o em um browser.

## Desenvolvido com

* [Vue.js](https://vuejs.org/) - Framework javascript
* [Bulma](https://bulma.io/) - Framework CSS

