/* eslint-disable */

import { expect } from 'chai';
import { shallowMount } from '@vue/test-utils';
import App from '../../src/App.vue';
import notification from '../../src/components/notification.vue';
import insert from '../../src/components/insert.vue';
import list from '../../src/components/list.vue';

describe('App.vue', () => {
  it('render a div', function() {
    const wrapper = shallowMount(App);

    expect(wrapper.contains('div')).to.equal(true);
  });

  it('render its all components', function() {
    const wrapper = shallowMount(App);

    expect(wrapper.contains(insert)).to.equal(true);
    expect(wrapper.contains(list)).to.equal(true);
    expect(wrapper.contains(notification)).to.equal(true);
  });
});
