/* eslint-disable */

import chai from 'chai';
import chaiAsPromised from 'chai-as-promised';
import { mount } from '@vue/test-utils';
import insert from '../../src/components/insert.vue';

chai.use(chaiAsPromised);

const fs = require('fs-extra');

describe('Insert.vue', function(done) {
  before(() => {
    const data = {
      todos: [
        {
          id: 1,
          order: 1,
          completed: true,
          description: 'Lavar carro',
        },
        {
          id: 2,
          order: 2,
          completed: false,
          description: 'Fazer compras',
        },
        {
          id: 3,
          order: 3,
          completed: false,
          description: 'Marcar médico',
        },
      ],
    };

    return new Promise((resolve, reject) => {
      fs.writeFile('tests/test-database.json', JSON.stringify(data))
        .then(() => {
          console.log('Unit test database restored');
          resolve();
        })
        .catch((err) => {
          console.log(err);
          reject(new Error('Writing test database error'));
        });
    });
  });

  it('Display error if task description is empty', () => {
    const wrapper = mount(insert);

    wrapper.vm.check();
    chai.expect(wrapper.emitted()).to.have.all.keys(['displayError']);
  });

  it('Insert one element', function() {
    const wrapper = mount(insert);
    const obj = {
      id: 4,
      order: 4,
      completed: false,
      description: 'Custom task',
    };
    chai.expect(wrapper.vm.insert(obj)).to.eventually.equal(obj);
  });
});
