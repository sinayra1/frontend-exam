/* eslint-disable */

import chai from 'chai';
import chaiAsPromised from 'chai-as-promised';
import {  shallowMount } from '@vue/test-utils';
import list from '../../src/components/list.vue';

chai.use(chaiAsPromised);

const fs = require('fs-extra');

describe('List.vue', function(done) {
  before(() => {
    const data = {
      todos: [
        {
          id: 1,
          order: 1,
          completed: true,
          description: 'Lavar carro',
        },
        {
          id: 2,
          order: 2,
          completed: false,
          description: 'Fazer compras',
        },
        {
          id: 3,
          order: 3,
          completed: false,
          description: 'Marcar médico',
        },
      ],
    };

    return new Promise((resolve, reject) => {
      fs.writeFile('tests/test-database.json', JSON.stringify(data))
        .then(() => {
          console.log('Unit test database restored');
          resolve();
        })
        .catch((err) => {
          console.log(err);
          reject(new Error('Writing test database error'));
        });
    });
  });

  it('database.json has at least one element', function() {
    const wrapper =  shallowMount(list);
    chai.expect(wrapper.vm.listTask()).to.eventually.have.length.gt(1);
  });

  it('update to complete the task of id 1', function() {
    const wrapper = shallowMount(list);
    const obj = {
      id: 1,
      order: 1,
      completed: true,
      description: 'Lavar carro',
    };
    chai.expect(wrapper.vm.updateTask(obj)).to.eventually.equal(obj);

    obj.completed = false;
    wrapper.vm.updateTask(obj);
  });

  it('delete task of id 1', function() {
    const wrapper = shallowMount(list);
    let len = 0;
    const obj = {
      id: 1,
      order: 1,
      completed: false,
      description: 'Lavar carro',
    };

    wrapper.vm.listTask().then((result) => {
      len = result.length;

      chai.expect(wrapper.vm.deleteTask(obj)).to.eventually.equal(len - 1);
    });
  });
});
