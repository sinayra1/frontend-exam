/* eslint-disable */

import { expect, assert } from 'chai';
import { shallowMount } from '@vue/test-utils';
import notification from '../../src/components/notification.vue';
import { EventBus } from '../../src/components/event-bus';

describe('Notification.vue', function() {
  it('Notification should not be render message if displayError event was not fire', () => {
    const wrapper = shallowMount(notification);

    const displayError = wrapper.find('b-notification-stub').text();
    if (displayError === '') {
      assert.isTrue(true);
    } else {
      expect(displayError.isVisible()).to.equal(false);
    }
  });

  it('Render custom error message', function() {
    const wrapper = shallowMount(notification);

    const error = {
      name: 'Custom error name',
      message: 'Custom error message',
    };
    const title = 'Custom error title';

    EventBus.$emit('displayError', title, error);

    const displayError = wrapper.find('b-notification-stub').text();

    if (displayError.includes(title) && displayError.includes(error.name) && displayError.includes(error.message)) {
      assert.isTrue(true);
    } else {
      assert.fail();
    }
  });
});
